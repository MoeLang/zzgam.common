﻿namespace ZZgam.UnityBaseTools
{
    public interface IPersistenceTool
    {
        bool Load<T>(string fileName, out T data);
        void Save(string fileName, object data);
    }
}

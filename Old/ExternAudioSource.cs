﻿using ZZgam.UnityBaseTools.Utils;
using System;
using UnityEngine;

namespace ZZgam.UnityBaseTools
{
    public static class ExternAudioSource
    {
        public static AudioSource Play(this AudioSource target, AudioClip audioClip)
        {
            target.clip = audioClip;
            target.Play();
            return target;
        }

        public static void OnPlayEnd(this AudioSource target, params Action[] actions)
        {
            if (target.clip != null && target.isPlaying)
            {
                Delay.Instance.DoAfterSeconds(target.clip.length, actions);
            }
        }
    }
}

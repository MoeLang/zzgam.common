﻿namespace ZZgam.UnityBaseTools
{
    public class ToolClass<T> where T : class
    {
        public static void SetNullIfEqual(ref T target, T source)
        {
            if (target != null && target.Equals(source))
            {
                target = null;
            }
        }
    }
}

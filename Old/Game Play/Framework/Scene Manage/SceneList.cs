﻿using ZZgam.UnityBaseTools.Design_Pattern.Singleton_Pattern;

namespace ZZgam.UnityBaseTools.Game_Play.Framework.Scene_Manage
{
    /// <summary>
    /// 一个典型的场景列表实现
    /// </summary>
    public class SceneList<T> : Singleton<T>, ISceneList where T : SceneList<T>, new()
    {
        public ISceneScript GetSceneMonoScript(SceneID sceneID)
        {
            return sceneID.GetSceneScript();
        }
    }
}

﻿using ZZgam.UnityBaseTools.Design_Pattern.Singleton_Pattern;
using ZZgam.UnityBaseTools.Event_Repack.ZZ_Action;

namespace ZZgam.UnityBaseTools.Game_Play.Framework.Scene_Manage
{
    /// <summary>
    /// 场景控制者
    /// 外界控制操作
    /// </summary>
    public class SceneDirector : SpeakerSingleton<SceneDirector>
    {
        /// <summary>
        /// 设置场景列表
        /// </summary>
        public ZZAction<ISceneList> SetSceneList = new ZZAction<ISceneList>();
        public ZZAction<SceneID> LoadSceneID = new ZZAction<SceneID>();

        protected override void SetupListeners(bool add)
        {
            SetSceneList.AutoListener(add, DoSetScriptID);
            LoadSceneID.AutoListener(add, DoLoadSceneID);
        }

        //ISceneList sceneList = null;
        private void DoSetScriptID(ISceneList sceneList)
        {
            SetSceneList.RemoveListener(DoSetScriptID);
            //this.sceneList = sceneList;
        }

        private void DoLoadSceneID(SceneID sceneID)
        {
            sceneID.GetSceneScript().LoadScene();
        }
    }
}

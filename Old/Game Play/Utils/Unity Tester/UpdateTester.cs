﻿using UnityEngine;

namespace ZZgam.UnityBaseTools.Game_Play.Utils.Unity_Tester
{
    public abstract class UpdateTester : MonoBehaviour
    {
        protected virtual void Update()
        {
            OnUpdate();
        }

        protected abstract void OnUpdate();
    }
}

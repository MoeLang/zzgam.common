﻿using UnityEngine;
using UnityEngine.UI;

namespace ZZgam.UnityBaseTools.Game_Play.Utils.Unity_Tester
{
    [RequireComponent(typeof(Button))]
    public abstract class ButtonTester : MonoBehaviour
    {
        protected virtual void Start()
        {
            GetComponent<Button>().onClick.AddListener(ButtonClicked);
        }

        protected abstract void ButtonClicked();
    }
}

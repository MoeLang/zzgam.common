﻿using System;
using System.Collections.Generic;
using UnityEngine;
using ZZgam.UnityBaseTools.Design_Pattern.Singleton_Pattern;
using ZZgam.UnityBaseTools.Event_Repack.ZZ_Action;
using ZZgam.UnityBaseTools.Event_Repack.ZZ_Func;

namespace ZZgam.UnityBaseTools.Game_Play.User_Input
{
    /// <summary>
    /// 输入按键绑定
    /// </summary>
    class BindingAxes : SpeakerSingleton<BindingAxes>
    {
        Dictionary<AxisID, HashSet<Action<float>>> dicAxes = new Dictionary<AxisID, HashSet<Action<float>>>();

        public ZZAction UpdateAxes = new ZZAction();

        public ZZFunc<AxisID, Action<float>, bool> BindAxisAction = new ZZFunc<AxisID, Action<float>, bool>();
        public ZZFunc<AxisID, Action<float>, bool> UnbindAxisAction = new ZZFunc<AxisID, Action<float>, bool>();

        protected override void SetupListeners(bool add)
        {
            UpdateAxes.AutoListener(add, InvokeUpdateAxes);
            BindAxisAction.AutoListener(add, InvokeBindAxisAction);
            UnbindAxisAction.AutoListener(add, InvokeUnbindAxisAction);
        }

        private void InvokeUpdateAxes()
        {
            foreach (var item in dicAxes)
            {
                float axisValue = Input.GetAxis(item.Key.ToString());
                foreach (var action in item.Value)
                {
                    action.Invoke(axisValue);
                }
            }
        }

        public bool InvokeBindAxisAction(AxisID axisID, Action<float> action)
        {
            if (dicAxes.ContainsKey(axisID))
            {
                if (!dicAxes[axisID].Contains(action))
                {
                    return dicAxes[axisID].Add(action);
                }
                else
                {
                    Debug.LogError(action.ToString() + "已经存在于绑定列表中");
                }
            }
            else
            {
                try
                {
                    dicAxes.Add(axisID, new HashSet<Action<float>>() { action });

                    InputRecieve.UpdateAxesEnable = true;

                    return true;
                }
                catch (Exception e)
                {
                    Debug.LogError(e);
                }
            }
            return false;
        }

        public bool InvokeUnbindAxisAction(AxisID axisID, Action<float> action)
        {
            if (dicAxes.ContainsKey(axisID))
            {
                if (dicAxes[axisID].Contains(action))
                {
                    bool ret = dicAxes[axisID].Remove(action);
                    if (dicAxes[axisID].Count == 0)
                    {
                        dicAxes.Remove(axisID);
                        if (dicAxes.Count == 0)
                        {
                            InputRecieve.UpdateAxesEnable = false;
                        }
                    }
                    return ret;
                }
                else
                {
                    Debug.LogErrorFormat("ZZAction<float>:{0}不存在!", action.ToString());
                }
            }
            else
            {
                Debug.LogErrorFormat("AxisID:{0}不存在!", axisID.ToString());
            }
            return false;
        }
    }
}

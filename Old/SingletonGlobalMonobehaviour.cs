﻿namespace ZZgam.UnityBaseTools
{
    public class SingletonGlobalMonobehaviour<T> : SingletonMonobehaviour<T> where T : SingletonGlobalMonobehaviour<T>
    {
        protected override void Awake()
        {
            base.Awake();
            DontDestroyOnLoad(gameObject);
        }
    }
}

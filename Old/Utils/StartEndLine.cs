﻿using UnityEngine;

namespace ZZgam.UnityBaseTools.Utils
{
    [ExecuteInEditMode]
    [RequireComponent(typeof(LineRenderer))]
    public class StartEndLine : MonoBehaviour
    {
        public Transform start;
        public Transform end;
        LineRenderer lineRenderer;
        private void Start()
        {
            lineRenderer = GetComponent<LineRenderer>();
            lineRenderer.useWorldSpace = true;
        }

        private void Update()
        {
            lineRenderer.SetPosition(0, start.position);
            lineRenderer.SetPosition(1, end.position);
        }
    }
}

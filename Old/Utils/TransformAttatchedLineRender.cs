﻿using UnityEngine;

namespace ZZgam.UnityBaseTools.Utils
{
    [ExecuteInEditMode]
    [RequireComponent(typeof(LineRenderer))]
    class TransformAttatchedLineRender : MonoBehaviour
    {
        public Transform[] points = null;
        LineRenderer lineRenderer;
        private void Start()
        {
            lineRenderer = GetComponent<LineRenderer>();
        }

        private void Update()
        {
            if (points != null)
            {
                lineRenderer.positionCount = points.Length;
                for (int i = 0; i < points.Length; i++)
                {
                    lineRenderer.SetPosition(i, points[i].position);
                }
            }
        }
    }
}

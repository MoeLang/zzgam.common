﻿using UnityEngine;

namespace Assets.ZZgam.Utils
{
    [ExecuteInEditMode]
    class ShowDistance : MonoBehaviour
    {
        public Transform p1 = null;
        public Transform p2 = null;
        public float distance;
        public float disX;
        public float disY;
        public float disZ;
        private void Update()
        {
            if (p1&&p2)
            {
                distance = Vector3.Distance(p1.position, p2.position);
                disX = p1.position.x - p2.position.x;
                disY = p1.position.y - p2.position.y;
                disZ = p1.position.z - p2.position.z;
            }
        }
    }
}

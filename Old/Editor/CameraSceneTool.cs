﻿using UnityEditor;
using UnityEditor.SceneManagement;
using UE = UnityEditor.Editor;

namespace Assets.ZZ.Scenes.Editor
{
    class CameraSceneTool : UE
    {
        public SceneAsset sceneCamera = null;

        //[MenuItem("ZZ/OpenCameraScenes")]
        static void OpenGlobalScenes()
        {
            var c = CreateInstance<CameraSceneTool>();
            if (c.sceneCamera)
            {
                EditorSceneManager.OpenScene(AssetDatabase.GetAssetPath(c.sceneCamera));
            }
        }
    }
}

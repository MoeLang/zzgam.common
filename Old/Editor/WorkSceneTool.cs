﻿using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;
using UnityEngine.SceneManagement;
using UE = UnityEditor.Editor;

namespace Assets.ZZ.Scenes.Editor
{
    class WorkSceneTool : UE
    {
        const string key_work = "workScenePath";
        //[MenuItem("ZZ/WorkScene/SetCurrentWorkScene")]
        static void SetCurrentWorkScene()
        {
            string path = SceneManager.GetActiveScene().path;
            Debug.Log("设置工作场景:" + path);
            PlayerPrefs.SetString(key_work, path);
        }

        //[MenuItem("ZZ/OpenWorkScene")]
        static void OpenWorkScene()
        {
            if (SceneManager.GetActiveScene().isDirty)
            {
                EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
            }
            string path = PlayerPrefs.GetString(key_work);
            Debug.Log("加载工作场景:" + path);
            EditorSceneManager.OpenScene(path);
        }


        const string key_main = "mainScenePath";
        //[MenuItem("ZZ/WorkScene/SetCurrentMainScene")]
        static void SetCurrentMainScene()
        {
            string path = SceneManager.GetActiveScene().path;
            Debug.Log("设置入口场景:" + path);
            PlayerPrefs.SetString(key_main, path);
        }

        //[MenuItem("ZZ/OpenMainScene")]
        static void OpenMainScene()
        {
            if (SceneManager.GetActiveScene().isDirty)
            {
                EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
            }
            string path = PlayerPrefs.GetString(key_main);
            Debug.Log("加载入口场景:" + path);
            EditorSceneManager.OpenScene(path);
        }
    }
}

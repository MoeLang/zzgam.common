﻿using UnityEngine;

namespace ZZgam.UnityBaseTools
{
    class SetActiveDifferent : MonoBehaviour
    {
        public Transform[] targets = null;
        private void OnEnable()
        {
            if (targets != null)
            {
                foreach (var item in targets)
                {
                    item.gameObject.SetActive(false);
                }
            }
        }
        private void OnDisable()
        {
            if (targets != null)
            {
                foreach (var item in targets)
                {
                    item.gameObject.SetActive(true);
                }
            }
        }
    }
}
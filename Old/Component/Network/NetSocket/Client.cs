﻿using Packages.zzgam.common.Component.Network.NetSocket;
using System.Net;
using System.Text;

namespace Assets.ZZgam
{
    public class Client : SocketPeel
    {
        public DataEventBytes OnDataRecvBytes = new DataEventBytes();
        public DataEventString OnDataRecvString = new DataEventString();

        public Client(IPAddress iPAddress, int port) : base()
        {
            Connect(iPAddress, port);
        }

        public Client(int port) : this(IPAddress.Loopback, port) { }

        protected override void OnRecieved(byte[] data)
        {
            if (OnDataRecvBytes != null)
            {
                OnDataRecvBytes.Invoke(data);
            }
            if (OnDataRecvString != null)
            {
                OnDataRecvString.Invoke(Encoding.UTF8.GetString(data));
            }
        }
    }
}
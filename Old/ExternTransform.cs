﻿using UnityEngine;

namespace ZZgam.UnityBaseTools
{
    public static class ExternTransform
    {
        /// <summary>
        /// 激活全部子物体
        /// </summary>
        /// <param name="target">目标Transform组件</param>
        /// <param name="needActive">是否激活</param>
        public static void ActiveAllSubGo(this Transform target, bool needActive)
        {
            for (int i = 0; i < target.childCount; i++)
            {
                target.GetChild(i).gameObject.SetActive(needActive);
            }
        }
    }
}

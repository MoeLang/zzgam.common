﻿using System;
using System.IO.Ports;

namespace Assets.ZZ.Utils.Devices
{
    [Serializable]
    public class SerialPortConfig
    {
        public SerialPortConfig(string portName)
        {
            this.portName = portName;
        }
        public string portName;
        public int baudRate = 115200;
        public Parity parity = Parity.None;
        public int dataBits = 8;
        public StopBits stopBits = StopBits.One;
        public int timeOut = 0;
        public int gunID1;
        public int gunID2;
        public int gunID3A;
        public int gunID3B;
        public int knobID1;
        public int knobID2;
        public int knobID3;
    }
}

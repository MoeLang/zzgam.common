﻿using ZZgam.UnityBaseTools.Design_Pattern.Singleton_Pattern;
using ZZgam.UnityBaseTools.Event_Repack.ZZ_Action;
using ZZgam.UnityBaseTools.Event_Repack.ZZ_Func;

namespace Assets.ZZ.Scenes.Loaders.Level
{
    public class BM_Level : SingletonManager<BM_Level, MB_Level>
    {
        public enum LevelLoadModel
        {
            /// <summary>
            /// 添加场景并设置为活动场景
            /// 并释放之前的活动场景
            /// </summary>
            AddThisAndReleaseActive,

            /// <summary>
            /// 添加场景不设置为活动
            /// 且不删除任何场景
            /// </summary>
            AddThisNotActive,
        }

        public ZZAction<string, LevelLoadModel?> ELoadLevel = new ZZAction<string, LevelLoadModel?>();
        public ZZAction<string, LevelLoadModel> ELevelLoaded = new ZZAction<string, LevelLoadModel>();
        public ZZAction<string> EUnloadLevel = new ZZAction<string>();
        public ZZAction<string> ELevelUnloaded = new ZZAction<string>();

        public ZZFunc<string, bool> EIsLevelLoaded = new ZZFunc<string, bool>();
    }
}

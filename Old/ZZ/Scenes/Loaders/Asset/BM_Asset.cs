﻿using ZZgam.UnityBaseTools.Design_Pattern.Singleton_Pattern;
using ZZgam.UnityBaseTools.Event_Repack.ZZ_Func;
using UnityEngine;

namespace Assets.ZZ.Scenes.Loaders.Asset
{
    public class BM_Asset : SingletonManager<BM_Asset, MB_Asset>
    {
        public ZZFunc<string, AudioClip> ELoadResource_AudioClip = new ZZFunc<string, AudioClip>();
    }
}

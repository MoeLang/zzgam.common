﻿using ZZgam.UnityBaseTools.Design_Pattern.Singleton_Pattern;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.ZZ.Scenes.Loaders.Script
{
    public class MB_Script : SingletonBehaviour<MB_Script, BM_Script>
    {
        protected override void SetupListeners(bool add)
        {
            BM_Script.Instance.ELoadAutoSingleScript.AutoListener(add, LoadAutoSingleScript);
            BM_Script.Instance.ELoadScript.AutoListener(add, LoadScript);
            BM_Script.Instance.EUnloadScript.AutoListener(add, UnloadScript);
        }

        ICSharpScriptSelector scriptSelector;
        Dictionary<string, Component> scripts = new Dictionary<string, Component>();
        Component autoSingleScript;

        private void Start()
        {
            scriptSelector = transform.root.GetComponent<ICSharpScriptSelector>();
            StartCoroutine(Delay((s) => LoadAutoSingleScript(s)));
        }

        private IEnumerator Delay(Action<string> loadAutoSingleScript)
        {
            yield return null;
            loadAutoSingleScript.Invoke(string.Empty);
        }

        private void LoadAutoSingleScript(string scriptID = null)
        {
            if (autoSingleScript)
            {
                Destroy(autoSingleScript);
            }
            if (scriptID == null)
            {
                scriptID = string.Empty;
            }
            Type type = scriptSelector.GetScript(scriptID);
            if (type != null)
            {
                autoSingleScript = gameObject.AddComponent(type);
                BM_Script.Instance.EAutoSingleScriptLoaded.Invoke(scriptID);
            }
        }

        private void LoadScript(string scriptID)
        {
            if (scripts.ContainsKey(scriptID))
            {
                Debug.LogErrorFormat("脚本{0}已经加载", scriptID);
            }
            else
            {
                Type type = scriptSelector.GetScript(scriptID);
                scripts.Add(scriptID, gameObject.AddComponent(type));
                BM_Script.Instance.EScriptLoaded.Invoke(scriptID);
            }
        }

        private void UnloadScript(string scriptID)
        {
            if (scripts.ContainsKey(scriptID))
            {
                Destroy(scripts[scriptID]);
                scripts.Remove(scriptID);
                BM_Script.Instance.EScriptUnloaded.Invoke(scriptID);
            }
            else
            {
                Debug.LogErrorFormat("没有加载脚本{0}", scriptID);
            }
        }
    }
}

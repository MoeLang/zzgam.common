﻿using UnityEngine;

namespace Assets.ZZgam
{
    public static class ColorEx
    {
        public static Color Create(this Color color, int rgb)
        {
            int r = 0, g = 0, b = 0;
            r = (rgb & 16777215) / 256 / 256;
            g = (rgb & 65535) / 256;
            b = rgb & 255;
            return Color.clear.Create(r, g, b);
        }

        public static Color Create(this Color color, int r, int g, int b)
        {
            return new Color((float)r / 256, (float)g / 256, (float)b / 256);
        }
    }
}
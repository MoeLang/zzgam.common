﻿namespace ZZgam.UnityBaseTools.Design_Pattern.Singleton_Pattern
{
    public abstract class SpeakerSingleton<T> : Singleton<T> where T : SpeakerSingleton<T>, new()
    {
        public SpeakerSingleton()
        {
            SetupListeners(true);
        }

        ~SpeakerSingleton()
        {
            SetupListeners(false);
        }

        protected abstract void SetupListeners(bool add);
    }
}

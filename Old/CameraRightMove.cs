﻿using UnityEngine;
using UnityEngine.XR;

namespace ZZgam.UnityBaseTools
{
    class CameraRightMove : MonoBehaviour
    {
        public float moveSpeed = 1;

        public float rotation_H_speed = 1;
        public float rotation_V_speed = 1;
        public float max_up_angle = 80;
        public float max_down_angle = -60;


        float current_rotation_H;
        float current_rotation_V;
        bool enableSimulate;
        private void Start()
        {
            enableSimulate = !XRSettings.enabled;
        }
        void LateUpdate()
        {
            if (enableSimulate)
            {
                if (Input.GetMouseButton(1))
                {
                    current_rotation_H += Input.GetAxis("Mouse X") * rotation_H_speed;
                    current_rotation_V += Input.GetAxis("Mouse Y") * rotation_V_speed;
                    current_rotation_V = Mathf.Clamp(current_rotation_V, max_down_angle, max_up_angle);
                    transform.localEulerAngles = new Vector3(-current_rotation_V, current_rotation_H, 0f);
                }
                transform.Translate(new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Up"), Input.GetAxis("Vertical")) * Time.deltaTime * moveSpeed);
            }
        }
    }
}

﻿using UnityEngine;
using UnityEngine.Rendering;

namespace ZZgam.UnityBaseTools
{
    [RequireComponent(typeof(MeshFilter))]
    public abstract class ProcessMesh : MonoBehaviour
    {
        public string meshName = null;
        public IndexFormat indexFormat = IndexFormat.UInt16;
        protected Vector3[] vertices = null;
        protected int[] triangles = null;
        protected Vector2[] uv = null;
        protected Vector4[] tangents = null;

        Mesh mesh = null;
        MeshFilter meshFilter = null;

        protected virtual void Awake()
        {
            meshFilter = GetComponent<MeshFilter>();
        }

        protected abstract void InitMesh();

        protected virtual void UpdateMesh()
        {
            mesh = new Mesh
            {
                name = meshName,
                indexFormat = indexFormat,
                vertices = vertices,
                triangles = triangles,
                uv = uv,
                tangents = tangents
            };
            mesh.RecalculateNormals();
            mesh.RecalculateBounds();
            meshFilter.mesh = mesh;
        }
    }
}

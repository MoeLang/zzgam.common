﻿using UnityEngine;

namespace ZZgam.UnityBaseTools
{
    public interface IOnStateExitReceiver
    {
        void OnStateExit(AnimatorStateInfo stateInfo, int layerIndex);
    }
}
